# My Uberspace manual

This repository contains a list of how-to and manual docs for use cases I have.

## Mailing Lists 

Uberspace offers Mailinglists using ezmlm but currently only in **UberSpace version 6** (US6). Therefore this manueal references all the docs for US6.

### 1. Get Uberspace Account

- [Register Account](https://uberspace.de/register). Ensure to unselect the Uberspacke 7 feature. 
- [Upload SSH Key](https://uberspace.de/dashboard/authentication)
- SSH into your webspace using the key (Find the SSH Host at the [Datenblatt page](https://uberspace.de/dashboard/datasheet))

### 2. Configure Domains

- Follow the [instruction how to configure an domain for email from the user manual](https://wiki.uberspace.de/domain:verwalten)
- Set MX and A and AAA record forthe domain using the values from the CLI output

### 3. Configure Mailing List

#### Creation

Assuming the localpart of the mailinglist is `test` and the domain is `domain.tld` and the email address of the moderator is `mod@example.org`.

```
mkdir ~/Mailinglist
ezmlm-make ~/Mailinglist/test ~/.qmail-test test domain.tld
ezmlm-make -+ -C /etc/ezmlm/de -aBDfGHiJlMNOprstuXY ~/Mailinglist/test ~/.qmail-test test domain.tld
ezmlm-sub ~/Mailinglist/test mod mod@example.org
ezmlm-sub ~/Mailinglist/test mod@example.org
```

#### Set Prefix

```
echo '[SUBJECT PREFIX]' > ~/Mailinglist/test/prefix
```

#### Set Trailer

```
joe ~/Mailinglist/test/text/trailer
```

#### Import Subscribers from file

```
ezmlm-sub ~/Maillist/test < 'listmembers'.txt
```

#### Resources

- [ezmlm manual page in the uberspace wiki](https://wiki.uberspace.de/mail:ezmlmidx)
- [Mailinglist command generator tool](https://nanooq.github.io/ezmlm-make-options-generator/)
- [Blogpost](https://cbmainz.de/ezmlm-idx-mailingliste-auf-uberspace-einrichten/)